package io.gitlab.leewonjong29cm

import android.text.TextUtils.TruncateAt
import android.widget.TextView

/**
 * Created by leewonjong@29cm.co.kr on 2022-09-26
 */
fun TextView.getEllipsizedText(ellipsize: TruncateAt = TruncateAt.END): String {
    if (text.isNullOrEmpty()) {
        return ""
    }
    return when (ellipsize) {
        TruncateAt.START -> getEllipsizedTextStart()
        TruncateAt.END -> getEllipsizedTextEnd()
        else -> ""
    }
}

private fun TextView.getEllipsizedTextEnd(): String {
    val end = text.length - (layout?.getEllipsisCount(maxLines - 1) ?: 0)
    return text.toString().substring(0, end)
}

private fun TextView.getEllipsizedTextStart(): String {
    val start = (layout?.getEllipsisCount(maxLines - 1) ?: 0)
    return text.toString().substring(start, text.length)
}